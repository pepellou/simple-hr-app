USE hrapp;

INSERT INTO `departments` (`id`, `name`) VALUES
(1,  'Production'),
(2,  'Research and Development'),
(3,  'Purchasing'),
(4,  'Marketing'),
(5,  'Human Resource Management'),
(6,  'Accounting and Finance'),
(7,  'Quality Assurance'),
(8,  'Sales'),
(9,  'Operations'),
(10, 'Technical Support'),
(11, 'Communications'),
(12, 'IT'),
(13, 'Legal');
