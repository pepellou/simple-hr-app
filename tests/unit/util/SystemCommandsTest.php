<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

require_once(dirname(__FILE__).'/../../../src/util/SystemCommands.php');

final class SystemCommandsTest extends TestCase
{

    public function testCanGetOutputOfRunningACommand(): void
    {
        $this->assertEquals(
            __FILE__,
            SystemCommands::run('ls ' . __FILE__)
        );

        $this->assertEquals(
            '5 + 3 = 8',
            SystemCommands::run('echo 5 + 3 = `expr 5 + 3`')
        );
    }

    public function testExceptionIsThrownWhenCommandError(): void
    {
        $this->expectException(CommandException::class);
        $this->expectExceptionMessage('asd: command not found');
        $this->expectExceptionCode(127);

        $this->assertEquals(
            __FILE__,
            SystemCommands::run('asd')
        );
    }

}
