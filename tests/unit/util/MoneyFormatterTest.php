<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

require_once(dirname(__FILE__).'/../../../src/util/MoneyFormatter.php');

final class MoneyFormatterTest extends TestCase
{

    /**
     * @dataProvider amountsProvider
     */
    public function testFormatsAnyAmountOfDollars($amount, $expected): void
    {
        $this->assertEquals(
            '$ ' . $expected,
            MoneyFormatter::format($amount)
        );
    }

    /**
     * @dataProvider amountsProvider
     */
    public function testCanUseAnyCurrencySymbol($amount, $expected): void
    {
        $theCurrency = 'EUR';

        $this->assertEquals(
            $theCurrency . ' ' . $expected,
            MoneyFormatter::format($amount, $theCurrency)
        );
    }

    public function amountsProvider()
    {
        return [
            '30,300.00' => [ 3030000, '30,300.00' ],
            '45,300.00' => [ 4530000, '45,300.00' ],
            '12,345.67' => [ 1234567, '12,345.67' ],
            ' 1,000.00' => [  100000,  '1,000.00' ],
            '   999.99' => [   99999,    '999.99' ],
            '     9.99' => [     999,      '9.99' ],
            '     0.00' => [       0,      '0.00' ]
        ];
    }

}
