<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

require_once(dirname(__FILE__).'/../../src/util/SystemCommands.php');

final class DeleteDepartmentTest extends TestCase
{

    private $db_dir;
    private $api_dir;

    /**
     * @before
     */
    public function setup(): void
    {
        $this->db_dir  = dirname(__FILE__).'/../../db';
        $this->api_dir = dirname(__FILE__).'/../../api';

        SystemCommands::run( "$this->db_dir/down" );
        SystemCommands::run( "$this->db_dir/up"   );
        SystemCommands::run( "$this->db_dir/data" );

        SystemCommands::run(
            "echo '" . json_encode([ "name" => "An Empty Department" ]) . "'"
            . " | php $this->api_dir/department/new.php"
        );

        SystemCommands::run(
            "echo '" . json_encode([ "name" => "Another Empty Department" ]) . "'"
            . " | php $this->api_dir/department/new.php"
        );
    }

    /**
     * @dataProvider someEmptyDepartments
     */
    public function testCanDeleteEmptyDepartment($id, $name): void
    {
        $the_department = json_decode(
            SystemCommands::run("echo ${id} | php $this->api_dir/department/one.php")
        );

        $this->assertEquals($id,   $the_department->id);
        $this->assertEquals($name, $the_department->name);

        $result = json_decode(SystemCommands::run(
            "echo '" . json_encode($the_department) . "'"
            . " | php $this->api_dir/department/delete.php"
        ));

        $this->assertEquals(
            'The department was deleted!',
            $result->message
        );

        $the_department = json_decode(
            SystemCommands::run("echo ${id} | php $this->api_dir/department/one.php")
        );

        $this->assertEquals('Department does not exist.', $the_department->error);
    }

    /**
     * @dataProvider someNonEmptyDepartments
     */
    public function testCannotDeleteNonEmptyDepartment($id, $name): void
    {
        $the_department = json_decode(
            SystemCommands::run("echo ${id} | php $this->api_dir/department/one.php")
        );

        $this->assertEquals($id,   $the_department->id);
        $this->assertEquals($name, $the_department->name);

        $result = json_decode(SystemCommands::run(
            "echo '" . json_encode($the_department) . "'"
            . " | php $this->api_dir/department/delete.php"
        ));

        $this->assertEquals(
            'Unable to delete department.',
            $result->error
        );

        $the_department = json_decode(
            SystemCommands::run("echo ${id} | php $this->api_dir/department/one.php")
        );

        $this->assertEquals($id,   $the_department->id);
        $this->assertEquals($name, $the_department->name);
    }

    public function someEmptyDepartments()
    {
        return [
            'An Empty Department'      => [ 14, 'An Empty Department'      ],
            'Another Empty Department' => [ 15, 'Another Empty Department' ]
        ];
    }

    public function someNonEmptyDepartments()
    {
        return [
            'Production'             => [  1, 'Production'             ],
            'Purchasing'             => [  3, 'Purchasing'             ],
            'Accounting and Finance' => [  6, 'Accounting and Finance' ],
            'Legal'                  => [ 13, 'Legal'                  ]
        ];
    }

}
