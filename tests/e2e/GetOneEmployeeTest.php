<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

require_once(dirname(__FILE__).'/../../src/util/SystemCommands.php');

final class GetOneEmployeeTest extends TestCase
{

    private $db_dir;
    private $api_dir;

    /**
     * @before
     */
    public function setup(): void
    {
        $this->db_dir  = dirname(__FILE__).'/../../db';
        $this->api_dir = dirname(__FILE__).'/../../api';

        SystemCommands::run( "$this->db_dir/down" );
        SystemCommands::run( "$this->db_dir/up"   );
        SystemCommands::run( "$this->db_dir/data" );
    }

    /**
     * @dataProvider someEmployees
     */
    public function testCanGetASingleEmployee($id, $name, $salary, $raw_salary, $department): void
    {
        $the_employee = json_decode(
            SystemCommands::run("echo ${id} | php $this->api_dir/employee/one.php")
        );

        $this->assertEquals($id,         $the_employee->id);
        $this->assertEquals($name,       $the_employee->name);
        $this->assertEquals($salary,     $the_employee->salary);
        $this->assertEquals($department, $the_employee->department_name);
    }

    /**
     * @dataProvider someEmployees
     */
    public function testReturnsSalaryAlsoInRawFormat($id, $name, $salary, $raw_salary, $department): void
    {
        $the_employee = json_decode(
            SystemCommands::run("echo ${id} | php $this->api_dir/employee/one.php")
        );

        $this->assertEquals($salary,     $the_employee->salary);
        $this->assertEquals($raw_salary, $the_employee->salary_raw);
    }

    public function someEmployees()
    {
        return [
            'Cairo Peterson'         => [  8, 'Cairo Peterson', '$ 56,400.00', 5640000, 'Research and Development'  ],
            'Logan Freeman'          => [ 11, 'Logan Freeman',  '$ 39,500.00', 3950000, 'Purchasing'                ],
            'Daniele Riggs'          => [ 24, 'Daniele Riggs',  '$ 74,200.00', 7420000, 'Human Resource Management' ],
            'Rhian Simmonds'         => [ 46, 'Rhian Simmonds', '$ 58,200.00', 5820000, 'Technical Support'         ],
            'Betty Love'             => [ 62, 'Betty Love',     '$ 58,200.00', 5820000, 'Legal'                     ]
        ];
    }

}
