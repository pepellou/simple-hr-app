<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

require_once(dirname(__FILE__).'/../../src/util/SystemCommands.php');

final class CreateDepartmentTest extends TestCase
{

    private $db_dir;
    private $api_dir;

    /**
     * @before
     */
    public function setup(): void
    {
        $this->db_dir  = dirname(__FILE__).'/../../db';
        $this->api_dir = dirname(__FILE__).'/../../api';

        SystemCommands::run( "$this->db_dir/down" );
        SystemCommands::run( "$this->db_dir/up"   );
        SystemCommands::run( "$this->db_dir/data" );
    }

    public function testCanCreateDepartment(): void
    {
        $all_departments = json_decode(
            SystemCommands::run("php $this->api_dir/department/all.php")
        );

        $this->assertEquals(
            13,
            count($all_departments->departments)
        );

        $theNewDepartment = [
            "name" => "ZZZ - A Department Name That Should Be The Last One Alphabetically"
        ];

        SystemCommands::run(
            "echo '" . json_encode($theNewDepartment) . "'"
            . " | php $this->api_dir/department/new.php"
        );

        $all_departments = json_decode(
            SystemCommands::run("php $this->api_dir/department/all.php")
        );

        $this->assertEquals(
            14,
            count($all_departments->departments)
        );

        $this->assertEquals(
            $theNewDepartment['name'],
            $all_departments->departments[13]->name
        );
    }

}
