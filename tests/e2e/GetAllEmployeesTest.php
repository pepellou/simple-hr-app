<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

require_once(dirname(__FILE__).'/../../src/util/SystemCommands.php');

final class GetAllEmployeesTest extends TestCase
{

    private $db_dir;
    private $api_dir;

    /**
     * @before
     */
    public function setup(): void
    {
        $this->db_dir  = dirname(__FILE__).'/../../db';
        $this->api_dir = dirname(__FILE__).'/../../api';

        SystemCommands::run( "$this->db_dir/down" );
        SystemCommands::run( "$this->db_dir/up"   );
        SystemCommands::run( "$this->db_dir/data" );
    }

    public function testCanGetAllEmployees(): void
    {
        $all_employees = json_decode(
            SystemCommands::run("php $this->api_dir/employee/all.php")
        );

        $this->assertTrue(
            is_array($all_employees->employees)
        );

        $this->assertEquals(
            65,
            count($all_employees->employees)
        );
    }

    public function testResultsAreInAlphabeticalOrder(): void
    {
        $all_employees = json_decode(
            SystemCommands::run("php $this->api_dir/employee/all.php")
        );

        $this->assertEquals(
            'Aadil Kerr',
            $all_employees->employees[0]->name
        );

        $this->assertEquals(
            'Abby Blanchard',
            $all_employees->employees[1]->name
        );

        $this->assertEquals(
            'Alaw Monroe',
            $all_employees->employees[2]->name
        );

        // ...

        $this->assertEquals(
            'Willa Jordan',
            $all_employees->employees[64]->name
        );
    }

    public function testWhenNoEmployeesItReturnsEmptyArray(): void
    {
        SystemCommands::run( "$this->db_dir/down" );
        SystemCommands::run( "$this->db_dir/up"   );

        $all_employees = json_decode(
            SystemCommands::run("php $this->api_dir/employee/all.php")
        );

        $this->assertTrue(
            is_array($all_employees->employees)
        );

        $this->assertEquals(
            0,
            count($all_employees->employees)
        );
    }

    public function testEachEmployeeHasTheirDepartment(): void
    {
        $all_employees = json_decode(
            SystemCommands::run("php $this->api_dir/employee/all.php")
        );

        $this->assertEquals(
            'Technical Support',
            $all_employees->employees[0]->department
        );

        $this->assertEquals(
            'Communications',
            $all_employees->employees[1]->department
        );

    }

    public function testEachEmployeeHasAHumanReadableSalary(): void
    {
        $all_employees = json_decode(
            SystemCommands::run("php $this->api_dir/employee/all.php")
        );

        $this->assertEquals(
            '$ 57,900.00',
            $all_employees->employees[0]->salary
        );

        $this->assertEquals(
            '$ 30,300.00',
            $all_employees->employees[1]->salary
        );

    }

}
