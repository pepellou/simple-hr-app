<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

require_once(dirname(__FILE__).'/../../src/util/SystemCommands.php');

final class GetOneDepartmentTest extends TestCase
{

    private $db_dir;
    private $api_dir;

    /**
     * @before
     */
    public function setup(): void
    {
        $this->db_dir  = dirname(__FILE__).'/../../db';
        $this->api_dir = dirname(__FILE__).'/../../api';

        SystemCommands::run( "$this->db_dir/down" );
        SystemCommands::run( "$this->db_dir/up"   );
        SystemCommands::run( "$this->db_dir/data" );
    }

    /**
     * @dataProvider someDepartments
     */
    public function testCanGetASingleDepartment($id, $name): void
    {
        $the_department = json_decode(
            SystemCommands::run("echo ${id} | php $this->api_dir/department/one.php")
        );

        $this->assertEquals($id,   $the_department->id);
        $this->assertEquals($name, $the_department->name);
    }

    public function someDepartments()
    {
        return [
            'Production'             => [  1, 'Production'             ],
            'Purchasing'             => [  3, 'Purchasing'             ],
            'Accounting and Finance' => [  6, 'Accounting and Finance' ],
            'Legal'                  => [ 13, 'Legal'                  ]
        ];
    }

}
