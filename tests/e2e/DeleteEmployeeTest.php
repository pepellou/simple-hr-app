<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

require_once(dirname(__FILE__).'/../../src/util/SystemCommands.php');

final class DeleteEmployeeTest extends TestCase
{

    private $db_dir;
    private $api_dir;

    /**
     * @before
     */
    public function setup(): void
    {
        $this->db_dir  = dirname(__FILE__).'/../../db';
        $this->api_dir = dirname(__FILE__).'/../../api';

        SystemCommands::run( "$this->db_dir/down" );
        SystemCommands::run( "$this->db_dir/up"   );
        SystemCommands::run( "$this->db_dir/data" );
    }

    /**
     * @dataProvider someEmployees
     */
    public function testCanDeleteEmptyEmployee($id, $name, $salary, $department): void
    {
        $the_employee = json_decode(
            SystemCommands::run("echo ${id} | php $this->api_dir/employee/one.php")
        );

        $this->assertEquals($id,         $the_employee->id);
        $this->assertEquals($name,       $the_employee->name);
        $this->assertEquals($salary,     $the_employee->salary);
        $this->assertEquals($department, $the_employee->department_name);

        $result = json_decode(SystemCommands::run(
            "echo '" . json_encode($the_employee) . "'"
            . " | php $this->api_dir/employee/delete.php"
        ));

        $this->assertEquals(
            'The employee was deleted!',
            $result->message
        );

        $the_employee = json_decode(
            SystemCommands::run("echo ${id} | php $this->api_dir/employee/one.php")
        );

        $this->assertEquals('Employee does not exist.', $the_employee->error);
    }

    public function someEmployees()
    {
        return [
            'Cairo Peterson'         => [  8, 'Cairo Peterson', '$ 56,400.00', 'Research and Development'  ],
            'Logan Freeman'          => [ 11, 'Logan Freeman',  '$ 39,500.00', 'Purchasing'                ],
            'Daniele Riggs'          => [ 24, 'Daniele Riggs',  '$ 74,200.00', 'Human Resource Management' ],
            'Rhian Simmonds'         => [ 46, 'Rhian Simmonds', '$ 58,200.00', 'Technical Support'         ],
            'Betty Love'             => [ 62, 'Betty Love',     '$ 58,200.00', 'Legal'                     ]
        ];
    }

}
