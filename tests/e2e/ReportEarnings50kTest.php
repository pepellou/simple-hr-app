<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

require_once(dirname(__FILE__).'/../../src/util/SystemCommands.php');

final class ReportEarnings50kTest extends TestCase
{

    private $db_dir;
    private $api_dir;

    private $expected_report;

    /**
     * @before
     */
    public function setup(): void
    {
        $this->db_dir  = dirname(__FILE__).'/../../db';
        $this->api_dir = dirname(__FILE__).'/../../api';

        SystemCommands::run( "$this->db_dir/down" );
        SystemCommands::run( "$this->db_dir/up"   );
        SystemCommands::run( "$this->db_dir/data" );

        $this->expected_report = [
            [
                'id' =>  6,
                'name' => 'Accounting and Finance',
                'employees' => [
                    'Kishan Herring',
                    'Sachin Humphries',
                    'Sharmin Lane',
                    'Willa Jordan'
                ]
            ],
            [
                'id' =>  5,
                'name' => 'Human Resource Management',
                'employees' => [
                    'Daniele Riggs',
                    'Ernest Amos',
                    'Reegan Erickson'
                ]
            ],
            [
                'id' => 12,
                'name' => 'IT',
                'employees' => [
                    'Avani Hull',
                    'Donell Watson',
                    'Kory Robson'
                ]
            ],
            [
                'id' => 13,
                'name' => 'Legal',
                'employees' => [
                    'Amari Mercado',
                    'Betty Love',
                    'Ibrar Travis',
                    'Kadeem Carty'
                ]
            ],
            [
                'id' =>  9,
                'name' => 'Operations',
                'employees' => [
                    'Amelie Ellison',
                    'Margaret Wardle',
                    'Selin Marshall'
                ]
            ],
            [
                'id' =>  7,
                'name' => 'Quality Assurance',
                'employees' => [
                    'Caden O\'Ryan',
                    'Jaidan Pitt',
                    'Rhona Cousins'
                ]
            ],
            [
                'id' => 10,
                'name' => 'Technical Support',
                'employees' => [
                    'Aadil Kerr',
                    'Rachael Perkins',
                    'Ravinder Jenkins',
                    'Rhian Simmonds',
                    'Siyana Haley'
                ]
            ]
        ];
    }

    public function testReturnsAllDepartmentsWithMoreThan2EmployeesEarningOver50k(): void
    {
        $all_departments = json_decode(
            SystemCommands::run("php $this->api_dir/report/earnings-50k.php")
        );

        $this->assertTrue(
            is_array($all_departments->departments)
        );

        $this->assertEquals(
            count($this->expected_report),
            count($all_departments->departments)
        );
    }

    public function testResultsAreInAlphabeticalOrder(): void
    {
        $all_departments = json_decode(
            SystemCommands::run("php $this->api_dir/report/earnings-50k.php")
        );

        $this->assertEquals(
            $this->expected_report[0]['name'],
            $all_departments->departments[0]->name
        );

        $this->assertEquals(
            $this->expected_report[1]['name'],
            $all_departments->departments[1]->name
        );

        $this->assertEquals(
            $this->expected_report[2]['name'],
            $all_departments->departments[2]->name
        );
    }

    public function testIncludesCountOfTheEmployeesOver50k(): void
    {
        $all_departments = json_decode(
            SystemCommands::run("php $this->api_dir/report/earnings-50k.php")
        );

        $this->assertEquals(
            count($this->expected_report[0]['employees']),
            $all_departments->departments[0]->num_selected_employees
        );

        $this->assertEquals(
            count($this->expected_report[1]['employees']),
            $all_departments->departments[1]->num_selected_employees
        );

        $this->assertEquals(
            count($this->expected_report[2]['employees']),
            $all_departments->departments[2]->num_selected_employees
        );
    }

    public function testIncludesNamesOfTheEmployeesOver50k_InAlphabeticalOrder(): void
    {
        $all_departments = json_decode(
            SystemCommands::run("php $this->api_dir/report/earnings-50k.php")
        );

        $this->assertEquals(
            implode(', ', $this->expected_report[0]['employees']),
            $all_departments->departments[0]->selected_employees
        );

        $this->assertEquals(
            implode(', ', $this->expected_report[1]['employees']),
            $all_departments->departments[1]->selected_employees
        );

        $this->assertEquals(
            implode(', ', $this->expected_report[2]['employees']),
            $all_departments->departments[2]->selected_employees
        );
    }

}
