<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

require_once(dirname(__FILE__).'/../../src/util/SystemCommands.php');

final class ReportHighestSalariesTest extends TestCase
{

    private $db_dir;
    private $api_dir;

    /**
     * @before
     */
    public function setup(): void
    {
        $this->db_dir  = dirname(__FILE__).'/../../db';
        $this->api_dir = dirname(__FILE__).'/../../api';

        SystemCommands::run( "$this->db_dir/down" );
        SystemCommands::run( "$this->db_dir/up"   );
        SystemCommands::run( "$this->db_dir/data" );
    }

    public function testReturnsAllDepartments(): void
    {
        $all_departments = json_decode(
            SystemCommands::run("php $this->api_dir/report/highest-salaries.php")
        );

        $this->assertTrue(
            is_array($all_departments->departments)
        );

        $this->assertEquals(
            13,
            count($all_departments->departments)
        );
    }

    public function testEachDepartmentHasItsHighestSalary(): void
    {
        $all_departments = json_decode(
            SystemCommands::run("php $this->api_dir/report/highest-salaries.php")
        );

        $this->assertEquals(
            '$ 74,700.00',
            $all_departments->departments[0]->highest_salary
        );

        $this->assertEquals(
            '$ 68,200.00',
            $all_departments->departments[1]->highest_salary
        );

        $this->assertEquals(
            '$ 74,200.00',
            $all_departments->departments[2]->highest_salary
        );

        // ...

        $this->assertEquals(
            '$ 74,700.00',
            $all_departments->departments[12]->highest_salary
        );
    }

    public function testIncludesNameOfEmployeeWithHighestSalary(): void
    {
        $all_departments = json_decode(
            SystemCommands::run("php $this->api_dir/report/highest-salaries.php")
        );

        $this->assertEquals(
            'Sachin Humphries',
            $all_departments->departments[0]->highest_salary_employee
        );

        $this->assertEquals(
            'Jessie Henson',
            $all_departments->departments[1]->highest_salary_employee
        );

        $this->assertEquals(
            'Daniele Riggs',
            $all_departments->departments[2]->highest_salary_employee
        );

        // ...

        $this->assertEquals(
            'Siyana Haley',
            $all_departments->departments[12]->highest_salary_employee
        );
    }

    public function testADepartmentWithNoEmployeesShouldReturnProperMessageInsteadOfTheNameOfTheEmployee(): void
    {
        $aNewDepartment = [
            "name" => "ZZZ - A Department Name That Should Be The Last One Alphabetically"
        ];

        SystemCommands::run(
            "echo '" . json_encode($aNewDepartment) . "'"
            . " | php $this->api_dir/department/new.php"
        );

        $all_departments = json_decode(
            SystemCommands::run("php $this->api_dir/report/highest-salaries.php")
        );

        $this->assertEquals(
            14,
            count($all_departments->departments)
        );

        $this->assertEquals(
            'no employees in this department',
            $all_departments->departments[13]->highest_salary_employee
        );
    }

}
