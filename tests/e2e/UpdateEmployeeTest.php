<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

require_once(dirname(__FILE__).'/../../src/util/SystemCommands.php');

final class UpdateEmployeeTest extends TestCase
{

    private $db_dir;
    private $api_dir;

    /**
     * @before
     */
    public function setup(): void
    {
        $this->db_dir  = dirname(__FILE__).'/../../db';
        $this->api_dir = dirname(__FILE__).'/../../api';

        SystemCommands::run( "$this->db_dir/down" );
        SystemCommands::run( "$this->db_dir/up"   );
        SystemCommands::run( "$this->db_dir/data" );
    }

    /**
     * @dataProvider someEmployees
     */
    public function testCanUpdateEmployee($id, $name, $salary, $department): void
    {
        $the_employee = json_decode(
            SystemCommands::run("echo ${id} | php $this->api_dir/employee/one.php")
        );

        $this->assertEquals($id,         $the_employee->id);
        $this->assertEquals($name,       $the_employee->name);
        $this->assertEquals($salary,     $the_employee->salary);
        $this->assertEquals($department, $the_employee->department_name);

        $theNewValues = [
            'id'         => $the_employee->id,
            'name'       => 'A different name',
            'salary'     => 123456789,
            'department' => 11
        ];
        $expectedSalary         = '$ 1,234,567.89';
        $expectedDepartmentName = 'Communications';

        SystemCommands::run(
            "echo '" . json_encode($theNewValues) . "'"
            . " | php $this->api_dir/employee/update.php"
        );

        $the_employee = json_decode(
            SystemCommands::run("echo ${id} | php $this->api_dir/employee/one.php")
        );

        $this->assertEquals($id,                     $the_employee->id);
        $this->assertEquals($theNewValues['name'],   $the_employee->name);
        $this->assertEquals($expectedSalary,         $the_employee->salary);
        $this->assertEquals($expectedDepartmentName, $the_employee->department_name);
    }

    public function someEmployees()
    {
        return [
            'Cairo Peterson'         => [  8, 'Cairo Peterson', '$ 56,400.00', 'Research and Development'  ],
            'Logan Freeman'          => [ 11, 'Logan Freeman',  '$ 39,500.00', 'Purchasing'                ],
            'Daniele Riggs'          => [ 24, 'Daniele Riggs',  '$ 74,200.00', 'Human Resource Management' ],
            'Rhian Simmonds'         => [ 46, 'Rhian Simmonds', '$ 58,200.00', 'Technical Support'         ],
            'Betty Love'             => [ 62, 'Betty Love',     '$ 58,200.00', 'Legal'                     ]
        ];
    }

}
