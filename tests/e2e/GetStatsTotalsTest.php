<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

require_once(dirname(__FILE__).'/../../src/util/SystemCommands.php');

final class GetStatsTotalsTest extends TestCase
{

    private $db_dir;
    private $api_dir;

    /**
     * @before
     */
    public function setup(): void
    {
        $this->db_dir  = dirname(__FILE__).'/../../db';
        $this->api_dir = dirname(__FILE__).'/../../api';

        SystemCommands::run( "$this->db_dir/down" );
        SystemCommands::run( "$this->db_dir/up"   );
        SystemCommands::run( "$this->db_dir/data" );
    }

    public function testCanGetAllStats(): void
    {
        $stats = json_decode(
            SystemCommands::run("php $this->api_dir/stats/all.php")
        );

        $this->assertEquals(
            13,
            $stats->total_departments
        );

        $this->assertEquals(
            65,
            $stats->total_employees
        );
    }

    public function testStatsAreProperlyUpdated(): void
    {
        $aNewDepartment = [
            "name" => "Any name for the department"
        ];

        SystemCommands::run(
            "echo '" . json_encode($aNewDepartment) . "'"
            . " | php $this->api_dir/department/new.php"
        );

        $stats = json_decode(
            SystemCommands::run("php $this->api_dir/stats/all.php")
        );

        $this->assertEquals(
            14,
            $stats->total_departments
        );

        $this->assertEquals(
            65,
            $stats->total_employees
        );
    }

}
