<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

require_once(dirname(__FILE__).'/../../src/util/SystemCommands.php');

final class GetAllDepartmentsTest extends TestCase
{

    private $db_dir;
    private $api_dir;

    /**
     * @before
     */
    public function setup(): void
    {
        $this->db_dir  = dirname(__FILE__).'/../../db';
        $this->api_dir = dirname(__FILE__).'/../../api';

        SystemCommands::run( "$this->db_dir/down" );
        SystemCommands::run( "$this->db_dir/up"   );
        SystemCommands::run( "$this->db_dir/data" );
    }

    public function testCanGetAllDepartments(): void
    {
        $all_departments = json_decode(
            SystemCommands::run("php $this->api_dir/department/all.php")
        );

        $this->assertTrue(
            is_array($all_departments->departments)
        );

        $this->assertEquals(
            13,
            count($all_departments->departments)
        );
    }

    public function testResultsAreInAlphabeticalOrder(): void
    {
        $all_departments = json_decode(
            SystemCommands::run("php $this->api_dir/department/all.php")
        );

        $this->assertEquals(
            'Accounting and Finance',
            $all_departments->departments[0]->name
        );

        $this->assertEquals(
            'Communications',
            $all_departments->departments[1]->name
        );

        $this->assertEquals(
            'Human Resource Management',
            $all_departments->departments[2]->name
        );

        // ...

        $this->assertEquals(
            'Technical Support',
            $all_departments->departments[12]->name
        );
    }

    public function testWhenNoDepartmentsItReturnsEmptyArray(): void
    {
        SystemCommands::run( "$this->db_dir/down" );
        SystemCommands::run( "$this->db_dir/up"   );

        $all_departments = json_decode(
            SystemCommands::run("php $this->api_dir/department/all.php")
        );

        $this->assertTrue(
            is_array($all_departments->departments)
        );

        $this->assertEquals(
            0,
            count($all_departments->departments)
        );
    }

    public function testEachDepartmentHasItsNumberOfEmployees(): void
    {
        $all_departments = json_decode(
            SystemCommands::run("php $this->api_dir/department/all.php")
        );

        $this->assertEquals(
            '5',
            $all_departments->departments[12]->num_employees
        );

        $theNewDepartment = [
            "name" => "ZZZ - A Department Name That Should Be The Last One Alphabetically"
        ];

        SystemCommands::run(
            "echo '" . json_encode($theNewDepartment) . "'"
            . " | php $this->api_dir/department/new.php"
        );

        $all_departments = json_decode(
            SystemCommands::run("php $this->api_dir/department/all.php")
        );

        $this->assertEquals(
            '0',
            $all_departments->departments[13]->num_employees
        );
    }

}
