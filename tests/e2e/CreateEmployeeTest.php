<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

require_once(dirname(__FILE__).'/../../src/util/SystemCommands.php');

final class CreateEmployeeTest extends TestCase
{

    private $db_dir;
    private $api_dir;

    /**
     * @before
     */
    public function setup(): void
    {
        $this->db_dir  = dirname(__FILE__).'/../../db';
        $this->api_dir = dirname(__FILE__).'/../../api';

        SystemCommands::run( "$this->db_dir/down" );
        SystemCommands::run( "$this->db_dir/up"   );
        SystemCommands::run( "$this->db_dir/data" );
    }

    public function testCanCreateEmployee(): void
    {
        $all_employees = json_decode(
            SystemCommands::run("php $this->api_dir/employee/all.php")
        );

        $this->assertEquals(
            65,
            count($all_employees->employees)
        );

        $theNewEmployee = [
            "name" => "ZZZ - A Employee Name That Should Be The Last One Alphabetically",
            "salary" => 2345600,
            "department" => 2
        ];

        SystemCommands::run(
            "echo '" . json_encode($theNewEmployee) . "'"
            . " | php $this->api_dir/employee/new.php"
        );

        $all_employees = json_decode(
            SystemCommands::run("php $this->api_dir/employee/all.php")
        );

        $this->assertEquals(
            66,
            count($all_employees->employees)
        );

        $this->assertEquals(
            $theNewEmployee['name'],
            $all_employees->employees[65]->name
        );
    }

}
