<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once dirname(__FILE__).'/../config/database.php';
include_once dirname(__FILE__).'/../entities/department.php';

$result = array();
$result["departments"] = array();

$department = new Department(Database::getInstance()->getConnection());
$rows = $department->all();
while ($row = $rows->fetch(PDO::FETCH_ASSOC)) {
    extract($row);

    array_push(
        $result["departments"],
        [
            "id" => $id,
            "name" => $name,
            "num_employees" => $num_employees
        ]
    );
}

http_response_code(200);

echo json_encode($result);
