<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');


include_once dirname(__FILE__).'/../config/database.php';
include_once dirname(__FILE__).'/../entities/department.php';

$result = array();
$result["departments"] = array();

$department = new Department(Database::getInstance()->getConnection());
$department->id = isset($_GET['id']) ? $_GET['id'] : trim(file_get_contents("php://stdin"));
$department->read();

if (!is_null($department->name)) {
    http_response_code(200);

    echo json_encode([
        'id'   => $department->id,
        'name' => $department->name
    ]);

} else {
    http_response_code(404);
    echo json_encode(["error" => "Department does not exist."]);
}
