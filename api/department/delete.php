<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 5");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once dirname(__FILE__).'/../config/database.php';
include_once dirname(__FILE__).'/../entities/department.php';

$result = array();
$result["departments"] = array();

$department = new Department(Database::getInstance()->getConnection());

$post_data = (object) $_POST;

if (count($_POST) == 0) {
    $post_data = json_decode(file_get_contents("php://stdin"));
}

$department->id = $post_data->id;

if ($department->delete()) {
    http_response_code(200);
    echo json_encode([
        "message" => "The department was deleted!"
    ]);
} else {
    http_response_code(503);
    echo json_encode([
        "error" => "Unable to delete department."
    ]);
}
