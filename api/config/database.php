<?php declare(strict_types=1);

require_once dirname(__FILE__)."/dbconfig.php";


class Database extends DBConfig
{

    private static $instance = null;

    public static function getInstance(): Database
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private $connection;

    public function getConnection(): PDO
    {
        return $this->connection;
    }

    private function __construct()
    {
        $this->connection = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
        $this->connection->exec("set names utf8");
        return $this->connection;
    }

}
