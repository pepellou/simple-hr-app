<?php

class Department {

    public $id;
    public $name;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function count()
    {
        $query = "SELECT
                    COUNT(*) as num
                FROM
                    " . $this->table_name;

        return $this->db->query($query)->fetchColumn();
    }

    public function all()
    {
        $query = "SELECT
                    d.id, d.name, (
                        SELECT COUNT(*)
                            FROM " . $this->employee_table_name . " e
                        WHERE e.department = d.id
                    ) as num_employees
                FROM
                    " . $this->table_name . " d
                ORDER BY
                    d.name ASC";

        $stmt = $this->db->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    public function read()
    {
        $query = "SELECT
                    d.id, d.name
                FROM
                    " . $this->table_name . " d
                WHERE
                    d.id = ?
                LIMIT
                    0,1";

        $stmt = $this->db->prepare($query);

        $stmt->bindParam(1, $this->id);

        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->name = $row['name'];
    }

    public function create()
    {
        $query = "INSERT INTO
                " . $this->table_name . "
                SET name=:name";

        $stmt = $this->db->prepare($query);

        $this->name = htmlspecialchars(strip_tags($this->name));

        $stmt->bindParam(":name", $this->name);

        return $stmt->execute();
    }

    public function update()
    {
        $query = "UPDATE
                " . $this->table_name . "
                SET
                    name = :name
                WHERE
                    id = :id";

        $stmt = $this->db->prepare($query);

        $this->name = htmlspecialchars(strip_tags($this->name));

        $stmt->bindParam(":id",   $this->id);
        $stmt->bindParam(":name", $this->name);

        return $stmt->execute();
    }

    public function delete()
    {
        if ($this->getNumOfEmployees() > 0) {
            return false;
        }

        $query = "DELETE
                FROM " . $this->table_name . "
                WHERE
                    id = :id";

        $stmt = $this->db->prepare($query);

        $stmt->bindParam(":id",   $this->id);

        return $stmt->execute();
    }

    public function getNumOfEmployees()
    {
        $query = "SELECT COUNT(*)
                FROM " . $this->employee_table_name . " e
                WHERE e.department = " . $this->id;

        return $this->db->query($query)->fetchColumn();
    }

    public function allWithHighestSalary()
    {
        $query = "SELECT
                    d.id, d.name,
                    (
                        SELECT MAX(e.salary)
                            FROM " . $this->employee_table_name . " e
                        WHERE e.department = d.id
                    ) as highest_salary,
                    (
                        SELECT (f.name)
                            FROM " . $this->employee_table_name . " f
                        WHERE f.department = d.id AND f.salary = highest_salary
                        LIMIT 1
                    ) as highest_salary_employee
                FROM
                    " . $this->table_name . " d
                ORDER BY
                    d.name ASC";

        $stmt = $this->db->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    public function allWithMoreThan2Earning50k()
    {
        $query = "SELECT
                    d.id, d.name,
                    (
                        SELECT COUNT(*)
                            FROM " . $this->employee_table_name . " e
                        WHERE e.department = d.id AND e.salary > 5000000
                    ) as num_selected_employees,
                    (
                        SELECT GROUP_CONCAT(f.name SEPARATOR ',')
                            FROM " . $this->employee_table_name . " f
                        WHERE f.department = d.id AND f.salary > 5000000
                        GROUP BY f.department
                    ) as selected_employees
                FROM
                    " . $this->table_name . " d
                HAVING num_selected_employees > 2
                ORDER BY
                    d.name ASC";

        $stmt = $this->db->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    private $db;
    private $table_name = "departments";
    private $employee_table_name = "employees";

}
