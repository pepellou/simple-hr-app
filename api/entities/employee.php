<?php

class Employee {

    public $id;
    public $name;
    public $salary;
    public $department;
    public $department_name;

    public function __construct($db){
        $this->db = $db;
    }

    public function count()
    {
        $query = "SELECT
                    COUNT(*) as num
                FROM
                    " . $this->table_name;

        return $this->db->query($query)->fetchColumn();
    }

    public function all(){
        $query = "SELECT
                    e.id, e.name, e.salary, d.name as department_name
                FROM
                    " . $this->table_name . " e,
                    " . $this->department_table_name . " d
                WHERE
                    e.department = d.id
                ORDER BY
                    e.name ASC";

        $stmt = $this->db->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    public function read()
    {
        $query = "SELECT
                    e.id, e.name, e.salary, e.department, d.name as department_name
                FROM
                    " . $this->table_name . " e,
                    " . $this->department_table_name . " d
                WHERE
                    e.id = ?
                    AND e.department = d.id
                LIMIT
                    0,1";

        $stmt = $this->db->prepare($query);

        $stmt->bindParam(1, $this->id);

        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->name            = $row['name'];
        $this->salary          = $row['salary'];
        $this->department      = $row['department'];
        $this->department_name = $row['department_name'];
    }

    public function create()
    {
        $query = "INSERT INTO
                " . $this->table_name . "
                SET name=:name, salary=:salary, department=:department";

        $stmt = $this->db->prepare($query);

        $this->name = htmlspecialchars(strip_tags($this->name));

        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":salary", $this->salary);
        $stmt->bindParam(":department", $this->department);

        return $stmt->execute();
    }

    public function update()
    {
        $query = "UPDATE
                " . $this->table_name . "
                SET
                    name = :name,
                    salary = :salary,
                    department = :department
                WHERE
                    id = :id";

        $stmt = $this->db->prepare($query);

        $this->name       = htmlspecialchars(strip_tags($this->name));

        $stmt->bindParam(":id",         $this->id);
        $stmt->bindParam(":name",       $this->name);
        $stmt->bindParam(":salary",     $this->salary);
        $stmt->bindParam(":department", $this->department);

        return $stmt->execute();
    }

    public function delete()
    {
        $query = "DELETE
                FROM " . $this->table_name . "
                WHERE
                    id = :id";

        $stmt = $this->db->prepare($query);

        $stmt->bindParam(":id",   $this->id);

        return $stmt->execute();
    }

    private $db;
    private $table_name = "employees";
    private $department_table_name = "departments";

}
