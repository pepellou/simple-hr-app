<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once dirname(__FILE__).'/../config/database.php';
include_once dirname(__FILE__).'/../entities/department.php';
include_once dirname(__FILE__).'/../entities/employee.php';

$db = Database::getInstance()->getConnection();
$department = new Department($db);
$employee =   new Employee($db);

$result = [
    'total_departments' => $department->count(),
    'total_employees'   => $employee->count()
];

http_response_code(200);

echo json_encode($result);
