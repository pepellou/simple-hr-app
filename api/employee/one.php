<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');


include_once dirname(__FILE__).'/../config/database.php';
include_once dirname(__FILE__).'/../entities/employee.php';
include_once dirname(__FILE__).'/../../src/util/MoneyFormatter.php';

$result = array();
$result["employees"] = array();

$employee = new Employee(Database::getInstance()->getConnection());
$employee->id = isset($_GET['id']) ? $_GET['id'] : trim(file_get_contents("php://stdin"));
$employee->read();

if (!is_null($employee->name)) {
    http_response_code(200);

    echo json_encode([
        'id'              => $employee->id,
        'name'            => $employee->name,
        'salary'          => MoneyFormatter::format($employee->salary),
        'salary_raw'      => $employee->salary,
        'department'      => $employee->department,
        'department_name' => $employee->department_name
    ]);

} else {
    http_response_code(404);
    echo json_encode(["error" => "Employee does not exist."]);
}
