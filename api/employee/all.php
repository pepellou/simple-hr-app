<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once dirname(__FILE__).'/../config/database.php';
include_once dirname(__FILE__).'/../entities/employee.php';
include_once dirname(__FILE__).'/../../src/util/MoneyFormatter.php';

$result = array();
$result["employees"] = array();

$employee = new Employee(Database::getInstance()->getConnection());
$rows = $employee->all();
while ($row = $rows->fetch(PDO::FETCH_ASSOC)) {
    extract($row);

    array_push(
        $result["employees"],
        [
            "id" => $id,
            "name" => $name,
            "salary" => MoneyFormatter::format($salary),
            "department" => $department_name
        ]
    );
}

http_response_code(200);

echo json_encode($result);
