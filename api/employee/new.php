<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 5");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");


include_once dirname(__FILE__).'/../config/database.php';
include_once dirname(__FILE__).'/../entities/employee.php';


$employee = new Employee(Database::getInstance()->getConnection());

$post_data = (object) $_POST;

if (count($_POST) == 0) {
    $post_data = json_decode(file_get_contents("php://stdin"));
}

if (!empty($post_data->name)
    && !empty($post_data->salary)
    && !empty($post_data->department)
) {
    $employee->name = $post_data->name;
    $employee->salary = $post_data->salary;
    $employee->department = $post_data->department;

    if ($employee->create()) {
        http_response_code(201);
        echo json_encode([
            "message" => "The employee has been created!"
        ]);
    } else {
        http_response_code(503);
        echo json_encode([
            "error" => "Unable to create employee."
        ]);
    }
} else {
    http_response_code(400);
    echo json_encode([
        "error" => "Unable to create employee. Name is missing."
    ]);
}
