<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 5");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once dirname(__FILE__).'/../config/database.php';
include_once dirname(__FILE__).'/../entities/employee.php';

$result = array();
$result["employees"] = array();

$employee = new Employee(Database::getInstance()->getConnection());

$post_data = (object) $_POST;

if (count($_POST) == 0) {
    $post_data = json_decode(file_get_contents("php://stdin"));
}

$employee->id = $post_data->id;

if ($employee->delete()) {
    http_response_code(200);
    echo json_encode([
        "message" => "The employee was deleted!"
    ]);
} else {
    http_response_code(503);
    echo json_encode([
        "error" => "Unable to delete employee."
    ]);
}
