<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once dirname(__FILE__).'/../config/database.php';
include_once dirname(__FILE__).'/../entities/department.php';
include_once dirname(__FILE__).'/../../src/util/MoneyFormatter.php';

$result = array();
$result["departments"] = array();

$department = new Department(Database::getInstance()->getConnection());
$rows = $department->allWithHighestSalary();
while ($row = $rows->fetch(PDO::FETCH_ASSOC)) {
    extract($row);

    array_push(
        $result["departments"],
        [
            "id" => $id,
            "name" => $name,
            "highest_salary" => MoneyFormatter::format($highest_salary),
            "highest_salary_employee" => (!is_null($highest_salary_employee) ? $highest_salary_employee : 'no employees in this department')
        ]
    );
}

http_response_code(200);

echo json_encode($result);
