<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once dirname(__FILE__).'/../config/database.php';
include_once dirname(__FILE__).'/../entities/department.php';
include_once dirname(__FILE__).'/../../src/util/MoneyFormatter.php';

$result = array();
$result["departments"] = array();

$department = new Department(Database::getInstance()->getConnection());
$rows = $department->allWithMoreThan2Earning50k();
while ($row = $rows->fetch(PDO::FETCH_ASSOC)) {
    extract($row);

    $employees = explode(',', $selected_employees);
    sort($employees);

    array_push(
        $result["departments"],
        [
            "id" => $id,
            "name" => $name,
            "num_selected_employees" => $num_selected_employees,
            "selected_employees" => implode(', ', $employees)
        ]
    );
}

http_response_code(200);

echo json_encode($result);
