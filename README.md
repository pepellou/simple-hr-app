# Simple HR app

Simple HR app with departments and employees.

Coding challenge: use PHP & MySQL to implement this, do not use an ORM or CMS.


## How to deploy to a server

Server requirements:

1. PHP 7+
2. MySQL database
3. git
4. SSH access without password (use `ssh-copy-id` if necessary)

Deploy steps:

1. In the server, wherever you want: `git clone https://gitlab.com/pepellou/simple-hr-app.git`
2. `cp deploy/.config.ini deploy/.config`
3. Edit `deploy/.config` to add server data
4. `deploy/deploy`
5. Run the necessary DB migrations in the server (will be automated in the future)

For now on, you'll only need step 4 to deploy.


## Set up local database

1. `cp db/.config.ini db/.config`
2. Edit `db/.config` to add credentials for the local MySQL root user
3. `cp api/config/config.php.ini api/config/dbconfig.php`
4. Edit `api/config/dbconfig.php` to add credentials for the local MySQL root user

Now you can run all migrations:

`db/up`

Or a specific migration by its number:

`db/up 1` or `db/up 0001`

You can also rollback a migration:

`db/down 1` or `db/down 0001`

Or rollback all of them:

`db/down`

## Run the tests

You can use `phpunit` at your discretion to run one or all of the tests.

If you want to run the whole suit (both end to end and unit tests), there's a `test` helper bash script:

`./test`

![Output of tests](img/screenshot-tests.png)

## Thanks

Bootstrap Theme: [NiceAdmin](https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/) by [BootstrapMade](https://bootstrapmade.com)
