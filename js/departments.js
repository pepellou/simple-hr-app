const API = 'http://www.bechess.com/simple-hr-app/api';

jQuery(document).ready(function(){

    $.get(API + '/department/all.php', function( data ) {
        const table = $('#departments_table tbody');
        for(let i in data.departments ) {
            var department = data.departments[i];
            table.append(
                '<tr>' +
                '  <td>' + department.name + '</td>' +
                '  <td>' + department.num_employees + '</td>' +
                '  <td>' +
                '    <div class="btn-group">' +
                '      <a class="btn btn-warning" href="edit_department.html?id=' + department.id + '"><i class="icon_pencil"></i></a>' +
                '      <a class="btn btn-danger delete-department" href="#" data-id="' + department.id + '" data-name="' + department.name + '"><i class="icon_close_alt2"></i></a>' +
                '    </div>' +
                '  </td>' +
                '</tr>'
            );
        }

        $('a.delete-department').click(function(e) {
            e.preventDefault();
            const $link = $(this);
            if (confirm('Are you sure to delete department "' + $link.data('name') + '"')) {
                $.ajax({
                    type: 'POST',
                    url: API + '/department/delete.php',
                    data: { id: $link.data('id') },
                    success: function( message ) {
                        $link.closest('tr').remove();
                    },
                    error: function( error ) {
                        alert('Can\'t delete this department. Make sure it has no employees and try again.');
                    }
                });
            }
        });
    });

});

