const API = 'http://www.bechess.com/simple-hr-app/api';

const getUrlParam = function(key) {
    const urlParams = window.location.search.substring(1);
    const urlVars = urlParams.split('&');
    for (let i = 0; i < urlVars.length; i++) {
        const param = urlVars[i].split('=');
        if (param[0] == key) {
            return param[1];
        }
    }
}

jQuery(document).ready(function(){

    const id = getUrlParam('id');

    $.get(API + '/department/one.php?id=' + id, function( data ) {
        $('#form_edit_department input[name=name]').val(data.name);
    });

    $('#form_edit_department').submit(function(e) {
        e.preventDefault();

        const department = {
            id: id,
            name: $('#form_edit_department input[name=name]').val()
        };
        $.ajax({
            type: 'POST',
            url: API + '/department/update.php',
            data: department,
            success: function( message ) {
                $('#success_edit_department').html(message.message);
                $('#success_edit_department').show();
            },
            error: function( error ) {
                const response = JSON.parse(error.responseText);
                $('#error_edit_department').html(response.error);
                $('#error_edit_department').show();
            }
        });

        return false;
    });

});

