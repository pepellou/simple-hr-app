const API = 'http://www.bechess.com/simple-hr-app/api';

jQuery(document).ready(function(){

    $('#form_add_department').submit(function(e) {
        e.preventDefault();

        const department = {
            name: $('#form_add_department input[name=name]').val()
        };
        $.ajax({
            type: 'POST',
            url: API + '/department/new.php',
            data: department,
            success: function( message ) {
                $('#success_add_department').html(message.message);
                $('#success_add_department').show();
            },
            error: function( error ) {
                const response = JSON.parse(error.responseText);
                $('#error_add_department').html(response.error);
                $('#error_add_department').show();
            }
        });

        return false;
    });

});

