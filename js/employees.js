const API = 'http://www.bechess.com/simple-hr-app/api';

jQuery(document).ready(function(){

    $.get(API + '/employee/all.php', function( data ) {
        const table = $('#employees_table tbody');
        for(let i in data.employees ) {
            var employee = data.employees[i];
            table.append(
                '<tr>' +
                '  <td>' + employee.name + '</td>' +
                '  <td>' + employee.department + '</td>' +
                '  <td>' + employee.salary + '</td>' +
                '  <td>' +
                '    <div class="btn-group">' +
                '      <a class="btn btn-danger delete-employee" href="#" data-id="' + employee.id + '" data-name="' + employee.name + '"><i class="icon_close_alt2"></i></a>' +
                '    </div>' +
                '  </td>' +
                '</tr>'
            );
        }

        $('a.delete-employee').click(function(e) {
            e.preventDefault();
            const $link = $(this);
            if (confirm('Are you sure to delete employee "' + $link.data('name') + '"')) {
                $.ajax({
                    type: 'POST',
                    url: API + '/employee/delete.php',
                    data: { id: $link.data('id') },
                    success: function( message ) {
                        $link.closest('tr').remove();
                    },
                    error: function( error ) {
                        const response = JSON.parse(error.responseText);
                        alert(response.error);
                    }
                });
            }
        });
    });

});

