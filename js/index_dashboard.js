const API = 'http://www.bechess.com/simple-hr-app/api';

jQuery(document).ready(function(){

    $.get(API + '/stats/all.php', function( data ) {
        $('#total_departments').html(data.total_departments);
        $('#total_employees').html(data.total_employees);
    });

});
