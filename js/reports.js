const API = 'http://www.bechess.com/simple-hr-app/api';

jQuery(document).ready(function(){

    $.get(API + '/report/highest-salaries.php', function( data ) {
        const table = $('#report_highest_salaries tbody');
        for (let i in data.departments) {
            var department = data.departments[i];
            table.append(
                '<tr>' +
                '  <td>' + department.name + '</td>' +
                '  <td>' + department.highest_salary + ' (' + department.highest_salary_employee + ')</td>' +
                '</tr>'
            );
        }
    });

    $.get(API + '/report/earnings-50k.php', function( data ) {
        const table = $('#report_earnings_50k tbody');
        for (let i in data.departments) {
            var department = data.departments[i];
            table.append(
                '<tr>' +
                '  <td>' + department.name + '</td>' +
                '  <td>' + department.num_selected_employees + ' (' + department.selected_employees + ')</td>' +
                '</tr>'
            );
        }
    });

});
