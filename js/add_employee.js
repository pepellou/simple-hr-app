const API = 'http://www.bechess.com/simple-hr-app/api';

jQuery(document).ready(function(){

    $.get(API + '/department/all.php', function( data ) {
        const select_department = $('#select_department');
        for(let i in data.departments ) {
            var department = data.departments[i];
            select_department.append(
                '<option value=' + department.id + '>' + department.name + '</option>'
            );
        }
    });

    $('#form_add_employee').submit(function(e) {
        e.preventDefault();

        const employee = {
            name: $('#form_add_employee input[name=name]').val(),
            salary: 100 * $('#form_add_employee input[name=salary]').val(),
            department: $('#form_add_employee select#select_department').val()
        };
        console.log(employee);
        $.ajax({
            type: 'POST',
            url: API + '/employee/new.php',
            data: employee,
            success: function( message ) {
                $('#success_add_employee').html(message.message);
                $('#success_add_employee').show();
            },
            error: function( error ) {
                const response = JSON.parse(error.responseText);
                $('#error_add_employee').html(response.error);
                $('#error_add_employee').show();
            }
        });

        return false;
    });

});

