<?php declare(strict_types=1);

final class MoneyFormatter
{

    public static function format($amount, $currency = '$'): string
    {
        return $currency . ' ' . number_format($amount / 100, 2);
    }

}
