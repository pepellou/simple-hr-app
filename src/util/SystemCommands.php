<?php declare(strict_types=1);

final class CommandException extends Exception
{
    public function __construct($message, $code)
    {
        parent::__construct($message, $code);
    }
}

final class SystemCommands
{

    public static function run($command): string
    {
        $output = [];

        exec("${command} 2>&1", $output, $status);

        $output_string = implode("\n", $output);

        if ($status != 0) {
            throw new CommandException($output_string, $status);
        }

        return $output_string;
    }

}
